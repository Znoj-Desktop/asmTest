#include "stdio.h";
#include "stdlib.h";

int main() {
	char myPromX;
	int myPromY;
	char* stringField = (char *) malloc(sizeof(char) * 11);

	__asm {
			mov eax, 1 //Napln� registr EAX hodnotou 1
			mov al, 0x6f //'o'
			mov eax, 0x35 //'5'
			mov al, al //'5'

			add eax, 4 //'9'
			sub eax, 4 //'9'
			add eax, '4' //'0x34'
			mov ah, al
			mov edx, eax
			sub eax, 5

			mov [edi], eax
			add dword ptr[edi], 1; //K 32bitov� hodnot� v pam�ti, jej� adresa je ulo�ena v EDI, p�i�te hodnotu 1
			mov eax, [edi]

			cmp eax, edx
			jbe myTEST
			jmp myEND
		myTEST:
			xor eax, eax
			jmp myEND
		myEND:
			mov eax, edx
			mov[myPromX], al
			mov[myPromY], eax

			xor ebx, ebx
			test ebx, 0
			jnz error
			jz ahojSvete
			jmp error

		ahojSvete:
			mov ebx, [stringField]
			mov [ebx], 'a'
			mov [ebx + 1], 'h'
			mov [ebx + 2], 'o'
			mov [ebx + 3], 'j'
			mov [ebx + 4], ' '
			add ebx, 5
			mov[ebx], 'S'
			inc ebx
			mov[ebx], 'v'
			inc ebx
			mov[ebx], 'e'
			inc ebx
			mov[ebx], 't'
			inc ebx
			mov[ebx], 'e'
			inc ebx
			mov[ebx], '\0'

			mov eax, 0x7FFFFFFF
			push eax
			add eax, 0x00000001
			jo end

		error:
			mov ebx, [stringField]
			mov [ebx], '\0'

		end:
			pop eax

	}


	printf("%c\n", myPromX);
	printf("%d\n", myPromY);
	for(int i = 0; i < 11; i++)
		printf("%c", stringField[i]);
	printf("\n");
	getchar();
	return 0;
}